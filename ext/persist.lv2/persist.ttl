# LV2 Persist Extension
# Copyright (C) 2010 Leonard Ritter <paniq@paniq.org>
# Copyright (C) 2010 David Robillard <d@drobilla.net>
# 
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

@prefix persist: <http://lv2plug.in/ns/ext/persist#> .
@prefix doap:    <http://usefulinc.com/ns/doap#> .
@prefix foaf:    <http://xmlns.com/foaf/0.1/> .
@prefix lv2:     <http://lv2plug.in/ns/lv2core#> .
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd:     <http://www.w3.org/2001/XMLSchema> .

<http://lv2plug.in/ns/ext/persist>
	a lv2:Specification ;
    doap:name "LV2 Persist" ;
    doap:developer [
        a foaf:Person ;
        foaf:name "Leonard Ritter" ;
        foaf:homepage <http://paniq.org> ;
    ] ;
    doap:maintainer [
        a foaf:Person ;
        foaf:name     "David Robillard" ;
        foaf:homepage <http://drobilla.net/> ;
        rdfs:seeAlso  <http://drobilla.net/drobilla.rdf>
    ] ;
	rdfs:comment """
This extension provides a mechanism for plugins to save and restore state
across instances, allowing hosts to save configuration/state/data with a
project or fully clone a plugin instance (including internal state).

Unlike ports, this extension allows plugins to save private state data.
The motivating ideal behind this extension is for the state of a plugin
instance to be entirely described by port values (as with all LV2 plugins) and
a key/value dictionary as defined by this extension.  This mechanism is simple,
yet sufficiently powerful to describe the state of very advanced plugins.

The "state" described by this extension is conceptually a single key/value
dictionary.  Keys are URIs, and values are typed-tagged blobs of any type.
The plugin provides a save and restore method for saving and restoring state.
To initiate a save or restore, the host calls these methods, passing a callback
to be used for saving or restoring a single key/value pair.  In this way, the
actual mechanism of saving and restoring state is completely abstract from the
plugin's perspective.

Because the state is a simple dictionary, hosts and plugins can work with state
easily (virtually all programming languages have an appropriate dictionary
type available).  Additionally, this format is simple and terse to serialise
in many formats (e.g. any RDF syntax, JSON, XML, key/value databases such as
BDB, etc.).  In particular, state can be elegantly described in a plugin's
Turtle description, which is useful for presets (among other things).
Note that these are all simply possibilities enabled by this simple data
model.  This extension defines only a few function prototypes and does not
impose any requirement to use a particular syntax, data structure, library,
or other implementation detail.  Hosts are free to work with plugin state
in whatever way is most appropriate for that host.

This extension makes it possible for plugins to save private data, but state is
not necessarily private, e.g. a plugin could have a public interface via ports
for manipulating internal state, which would be saved using this extension.
Plugins are strongly encouraged to represent all state change as modifications
of such key/value variables, to minimize implementation burden and enable
the many benefits of having a universal model for describing plugin state.
The use of URI keys prevents conflict and allows unrelated plugins to
meaningfully describe state changes.  Future extensions will describe a
dynamic mechanism for manipulating plugin state, as well as define various
keys likely to be useful to a wide range of plugins.

In pseudo code, a typical use case in a plugin is:
<pre>
static const char* const KEY_GREETING = "http://example.org/greeting";

void my_save(LV2_Handle                 instance,
             LV2_Persist_Store_Function store,
             void*                      callback_data)
{
    MyPlugin*   plugin   = (MyPlugin*)instance;
    const char* greeting = plugin->state->greeting;
    
    store(callback_data, KEY_GREETING,
        greeting, strlen(greeting) + 1,
        lv2_uri_map("http://lv2plug.in/ns/ext/atom#String"));
}

void my_restore(LV2_Handle                    instance,
                LV2_Persist_Retrieve_Function retrieve,
                void*                         callback_data)
{
    MyPlugin* plugin = (MyPlugin*)instance;

    size_t      size;
    uint32_t    type;
    const char* greeting = retrieve(callback_data, KEY_GREETING, &size, &type);

    if (greeting)
        plugin->state->greeting = greeting;
    else
        plugin->state->greeting = "Hello";
 
}
</pre>

Similarly, a typical use case in a host is:
<pre>
void store_callback(void*       callback_data,
                    const char* key,
                    const void* value,
                    size_t      size,
                    uint32_t    type)
{
    Map* state_map = (Map*)callback_data;
    state_map->insert(key, Value(value, size, type));
}

Map get_plugin_state(LV2_Handle instance)
{
    LV2_Persist* persist = instance.extension_data("http://lv2plug.in/ns/ext/persist");
    Map state_map;
    persist.save(instance, store_callback, &state_map);
    return state_map;
}
</pre>
""" .

persist:InstanceState
    a rdfs:Class ;
    rdfs:label "Plugin Instance State" ;
    rdfs:comment """
This class is used to express a plugin instance's state in RDF.  The key/value
properties of the instance form the predicate/object (respectively) of triples
with a persist:InstanceState as the subject (see persist:instanceState
for an example).  This may be used wherever it is useful to express a
plugin instance's state in RDF (e.g. for serialisation, storing in a model, or
transmitting over a network).  Note that this class is provided because it
may be useful for hosts, plugins, or extensions that work with instance state,
but its use is not required to support the LV2 Persist extension.
""" .


persist:instanceState
    a rdf:Property ;
    rdfs:range persist:InstanceState ;
    rdfs:comment """
Predicate to relate a plugin instance to an InstanceState.  This may be used
wherever the state of a particular plugin instance needs to be represented.
Note that the domain of this property is unspecified, since LV2 does not
define any RDF class for plugin instance.  This predicate may be used
wherever it makes sense to do so, e.g.:
<pre>
@prefix eg: &lt;http://example.org/&gt; .

&lt;plugininstance&gt; persist:instanceState [
    eg:somekey "some value" ;
    eg:someotherkey "some other value" ;
    eg:favouritenumber 2 .
]
</pre>
Note that this property is provided because it may be useful for hosts,
plugins, or extensions that work with instance state, but its use is not
required to support the LV2 Persist extension.
""" .
 